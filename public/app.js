let input = document.getElementById('userinput');
let vRadio = document.getElementById('vowel-function');
let vNum = document.getElementById('count');
let vPara = document.getElementById('vPara');
let cRadio = document.getElementById('consonant-function');
let cPara = document.getElementById('cPara');
let cNum = document.getElementById('count');
let chRadio = document.getElementById('char-function');
let chPara = document.getElementById('chPara');
let chNum = document.getElementById('count');
let countLabel = document.getElementById('count-label');
let template = document.querySelector('#letters-template');
let letterQuery = document.querySelector('.letter-query');
let overlay = document.querySelector('.overlay');
let calculateBtn = document.querySelector('#calculate');


let radios = document.querySelectorAll('input[type=radio]');

radios.forEach(radio => {
    radio.addEventListener('change', () => {
        if (radios[0].checked) {
            vPara.style.display = 'block';
            cPara.style.display = 'none';
            chPara.style.display = 'none';

        } else if (radios[1].checked) {
            cPara.style.display = 'block';
            vPara.style.display = 'none';
            chPara.style.display = 'none';

        } else if (radios[2].checked) {
            chPara.style.display = 'block';
            cPara.style.display = 'none';
            vPara.style.display = 'none';
        }
    });
});

let choosenFunction = () => {
    overlay.style.display = 'none';

    if (vRadio.checked) {
        let vLabel = 'Vowel Count: ';
        countLabel.innerText = vLabel;
        let word = input.value;
        let vowelCount = 0;
        let vowels = 'aeiouAEIOU'
        let storeVowels = [];

        /// Clear Dynamically added elements to prevent duplication.
        let dynamicElements = document.querySelectorAll('.dynamic-element');
        dynamicElements.forEach(element => element.remove());

        // iterate over each character and find vowel and store them in array
        for (let i = 0; i < word.length; i++) {
            if (vowels.includes(word[i])) {
                vowelCount++;
                storeVowels.push(word[i])
            }
        }
        vNum.innerText = vowelCount;


        for (let i = 0; i < storeVowels.length; i++) {
            let clone = document.importNode(template.content, true);
            let kbd = clone.querySelector('kbd');
            kbd.innerText = storeVowels[i];

            // Add a class to dynamically added elements
            clone.querySelector('.letters-wrapper').classList.add('dynamic-element');
            letterQuery.append(clone);
        }


    } else if (cRadio.checked) {
        let cLabel = 'Consonant Count: ';
        countLabel.innerText = cLabel;
        let word = input.value;
        let consonant = 0;
        let storeConsonants = [];
        const consonants = 'bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ';

        /// Clear Dynamically added elements to prevent duplication.
        let dynamicElements = document.querySelectorAll('.dynamic-element');
        dynamicElements.forEach(element => element.remove());

        for (let i = 0; i < word.length; i++) {
            if (consonants.includes(word[i])) {
                consonant++;
                storeConsonants.push(word[i])
            }
        }

        cNum.innerText = consonant;
        for (let i = 0; i < storeConsonants.length; i++) {
            let clone = document.importNode(template.content, true);
            let kbd = clone.querySelector('kbd');
            kbd.innerText = storeConsonants[i];

            // Add a class to dynamically added elements
            clone.querySelector('.letters-wrapper').classList.add('dynamic-element');

            letterQuery.append(clone);
        }

    } else if (chRadio.checked) {
        let chLabel = 'Character Count: ';
        countLabel.innerText = chLabel;
        let sentence = input.value;
        let charCount = sentence.split('').filter(char => char.trim() !== '').length;

        /// Clear Dynamically added elements to prevent duplication.
        let dynamicElements = document.querySelectorAll('.dynamic-element');
        dynamicElements.forEach(element => element.remove());

        chNum.innerText = charCount;
    } 
};